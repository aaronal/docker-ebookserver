# docker-ebookserver

Ebook-Server for IT-internal use. Using Calibre-Web. 
Currently, there is an installation of this running at [the osimage-server](http://osimage.pik-potsdam.de:8080). (Maybe, we will move that later)

## Install
You will need to get calibre-web first. Just run

```bash
git clone https://github.com/janeczku/calibre-web.git
```

and put it in this directory, so docker can find it and put it into the image.

Then run
```bash
docker build -t calibre-web .
```

to start the build. Afterwards you'll have a docker-image called **'calibre-web'** on your system. This directory gets copied into the image in the 
next step.

To run/spawn your container, you will need to run
```bash
docker run -p 8083:8083 -v /path/to/calibre/libary:/ebooks -v /path/to/calibre/config:/config --name myebookserver -d calibre-web
```

Now, you can controll your instance by the commands:
```bash
docker start myebookserver
docker stop myebookserver
```
Maybe you run this with a systemd-service-unit?
