# do a git clone first, make sure you've got calibre-web folder...


FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install -y python python-pip python-virtualenv
RUN apt-get install -y imagemagick libmagickwand-dev

# create a non-root user
RUN groupadd -g 999 appuser && \
    useradd -r -u 999 -g appuser appuser

RUN mkdir -p /app /ebooks /config
WORKDIR /app

# copy calibre-web to /app
COPY calibre-web/ /app/

# install the requirements
RUN pip install -r requirements.txt

# install the optional stuff
RUN pip install -r optional-requirements.txt

# install lxml
# RUN pip install lxml

# install policy.xml into /etc/ImageMagick
COPY policy.xml /etc/ImageMagick-6/policy.xml

# fix some permissions
RUN chown -R appuser /app

# symlink calibre-web config
RUN ln -s /config/app.db /app/app.db

EXPOSE 8083/tcp
USER appuser
CMD python cps.py
